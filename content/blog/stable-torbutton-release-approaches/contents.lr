title: Stable Torbutton Release Approaches
---
pub_date: 2008-07-07
---
author: mikeperry
---
tags:

torbutton
uglyrumors
---
categories: applications
---
_html_body:

<p>For those of you just tuning in: Over the past year, I have been the maintainer of the <a href="https://torbutton.torproject.org/dev/" rel="nofollow">Torbutton Firefox extension</a>, adding a number of features and security enhancements to transform Torbutton from a simple proxy switcher into a secure way to <a href="https://torbutton.torproject.org/dev/design/#requirements" rel="nofollow">fully isolate all browser state</a> from one proxy state to another and defend against all known <a href="https://torbutton.torproject.org/dev/design/#adversary" rel="nofollow">privacy and IP address leakage attacks</a>.</p>

<p>The release candidate phase of the extension started about a month ago, but with the release of Firefox 3 and Torbutton 1.2.0rc series occurring at the same time, we've hit a number of unexpected rough spots and snags. However, with the 1.2.0rc5 release of Torbutton, I'm pleased to report that the majority of those now seem to be <a href="https://torbutton.torproject.org/dev/CHANGELOG" rel="nofollow">behind us</a> (a few annoying <a href="https://torbutton.torproject.org/dev/design/#FirefoxBugs" rel="nofollow">Firefox bugs</a> notwithstanding).</p>

<p>Thanks to contributions from <a href="http://www.fdn.fr/~arenevier/" rel="nofollow">arno</a>, the Cookie Jar features now work with Firefox 3. They have even been improved to allow cookies to persist in memory-based jars across Tor toggle (as opposed to requiring Tor cookies to be written to disk to preserve them), which I personally already find very useful.</p>

<p>In addition, Torbutton is now much better about preserving users' custom Firefox preferences, including password and form fill preferences. Amusingly, the fact that we touch these preferences to protect users during Tor usage led to wild speculation on the <a href="https://addons.mozilla.org/en-US/firefox/addon/2275" rel="nofollow">addons.mozilla.org page</a> that we were using them to steal passwords and send user details to Alexa. Of course, simply grepping the <a href="https://tor-svn.freehaven.net/svn/torbutton/trunk" rel="nofollow">source code</a> for 'Alexa' and related IP addresses proves this to be false, but that didn't stop at least three people (or at least three sock puppets) from running with the rumor that Torbutton is a password stealer. Ignorance sure is contagious.</p>

<p>At any rate, after over a year since development began, it looks like we're finally getting really close to declaring Torbutton 1.2.0 'stable', which should coincide nicely with the upcoming Tor 0.2.0 stable release and bundles. It's been a long road!</p>

---
_comments:

<a id="comment-113"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-113" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 07, 2008</p>
    </div>
    <a href="#comment-113">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-113" class="permalink" rel="bookmark">Thanks! Useful button. Does</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks! Useful button. Does it works with Firefox 2.0.0.15?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-114"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-114" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">July 07, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-113" class="permalink" rel="bookmark">Thanks! Useful button. Does</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-114">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-114" class="permalink" rel="bookmark">Yes.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes. Both Firefox 2.0 and 3.0 are supported.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-116"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-116" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 08, 2008</p>
    </div>
    <a href="#comment-116">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-116" class="permalink" rel="bookmark">Does it work with Thunderbird ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, Torbutton is really useful for non-technical users of Firefox.</p>
<p>However, what about Thunderbird, is it supported or not ?<br />
I tried to install the extension from<br />
<a href="https://addons.mozilla.org/en-US/thunderbird/addon/2275" rel="nofollow">https://addons.mozilla.org/en-US/thunderbird/addon/2275</a><br />
The page says it's Thunderbird extension but Thunderbird 2.0.0.14 refuses to install it because it is incompatible.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-117" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">July 08, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-116" class="permalink" rel="bookmark">Does it work with Thunderbird ?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-117" class="permalink" rel="bookmark">addons.mozilla.org is a bit janky</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have no idea how that Thunderbird page was created. Well, I do have an idea how it was created, but I don't know why it still exists. Torbutton used to support basic proxy switching on Thunderbird back in the 1.0 days, but that support has been removed because it has not been analyzed for security. My developer tools page on addons.mozilla.org clearly lists Firefox support only, so I don't know why they didn't delete that Thunderbird listing. </p>
<p>I am not a Thunderbird user and unfortunately, I don't have time to analyze the security issues involved with toggling proxy settings in that app. It likely suffers from similar (but not identical) state and proxy leak issues with html mail, embedded images, javascript, plugins and automatic network access. My recommendation is to create a completely separate Thunderbird profile for your Tor accounts and use that instead of trying to toggle proxy settings. But if you really like to roll fast and loose with your IP, you could try another proxy switcher like ProxyButton, SwitchProxy or FoxyProxy (if any of those happen to support thunderbird).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-131" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 14, 2008</p>
    </div>
    <a href="#comment-131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-131" class="permalink" rel="bookmark">Blocking IPs in tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, how do i block unwanted IPs in TOR. Some one the tor nodes are malicious, they are injecting ActiveX in HTML code and modifies the data.</p>
<p>How do I block IPs doing this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-132"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-132" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 14, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-131" class="permalink" rel="bookmark">Blocking IPs in tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-132">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-132" class="permalink" rel="bookmark">re: Blocking IPs in tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Find the exit node and put it in "exclude nodes" in your torrc file.</p>
<p>However, you shouldn't be using a browser that honors activex through Tor.  Your anonymity can be compromised through activex.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-134"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-134" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 15, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-134">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-134" class="permalink" rel="bookmark">ExcludeNodes
nick,nick,nick,n</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ExcludeNodes<br />
nick,nick,nick,nick,nick</p>
<p>TOR won't start when I write this in my torrc. Is there other way to write it or can I block whole countries? Like in this one; <a href="http://archives.seul.org/or/talk/Jul-2006/msg00079.html" rel="nofollow">http://archives.seul.org/or/talk/Jul-2006/msg00079.html</a></p>
<p>Im running Windows btw.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-135"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-135" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 15, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-134" class="permalink" rel="bookmark">ExcludeNodes
nick,nick,nick,n</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-135">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-135" class="permalink" rel="bookmark">exclude nodes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There isn't an easy way to do this in windows.  In linux distros, there's tork.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-138"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-138" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 16, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-135" class="permalink" rel="bookmark">exclude nodes</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-138">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-138" class="permalink" rel="bookmark">Easier to block</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Easier to block with.</p>
<p>ReachableAddresses reject x.x.x.x/16:*,accept *:80,accept *:443,reject *:*</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-139"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-139" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2008</p>
    </div>
    <a href="#comment-139">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-139" class="permalink" rel="bookmark">is there somebody else who</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is there somebody else who experienced this bug...?</p>
<p>I can´t uninstall</p>
<p>the program leaves xp-install/unistall-window but remain in the tray and keep sending up configuration-window.</p>
<p>how can I get rid of this version?  (experimental vers)</p>
<p>I want the stable version instead...</p>
<p>best regards</p>
<p>mike</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-140"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-140" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2008</p>
    </div>
    <a href="#comment-140">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-140" class="permalink" rel="bookmark">I can´t uninstall-problem solved!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>problem solved!</p>
<p>did manage to uninstall at last...</p>
<p>best regards</p>
<p>mike</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-141"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-141" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 19, 2008</p>
    </div>
    <a href="#comment-141">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-141" class="permalink" rel="bookmark">ughhh</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't figure out how to submit when I have tor enabled. My form buttons don't work. I can't check email or anything.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://support-stage.mozilla.org/tiki-view_forum_thread.php?forumId=1&amp;comments_threshold=0&amp;comments_parentId=80128&amp;comments_offset=20&amp;comments_per_page=20&amp;thread_style=commentStyle_plain">Deja Dannielle (not verified)</a> said:</p>
      <p class="date-time">July 20, 2008</p>
    </div>
    <a href="#comment-142">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142" class="permalink" rel="bookmark">Router Login continuously reloads &quot;Status Webpage&quot; only.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><b>Router Login continuously reloads "Status Webpage" only.</b></p>
<blockquote><p>Firefox 3.01<br />
Dlink DGL-4300 Firmware v1.6<br />
Torbutton 1.2.0rc6<br />
Firebug 1.2.0b3</p></blockquote>
<p>Temporary Solution: Disable both Torbutton and Firebug Plug-in extensions.</p>
<p>Can someone please explain why these two plug-in extensions cause the Dlink router status webpage to continuously reload? Is this a Dlink, Firefox or plug-in extension issue here?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-145"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-145" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 22, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142" class="permalink" rel="bookmark">Router Login continuously reloads &quot;Status Webpage&quot; only.</a> by <a rel="nofollow" href="http://support-stage.mozilla.org/tiki-view_forum_thread.php?forumId=1&amp;comments_threshold=0&amp;comments_parentId=80128&amp;comments_offset=20&amp;comments_per_page=20&amp;thread_style=commentStyle_plain">Deja Dannielle (not verified)</a></p>
    <a href="#comment-145">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-145" class="permalink" rel="bookmark">It sounds like it&#039;s a dlink</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It sounds like it's a dlink issue.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2008</p>
    </div>
    <a href="#comment-148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-148" class="permalink" rel="bookmark">uninstall problem?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi ,I recently installed the stable download for windows xp but after I realised I had to use firefox I uninstalled it .Due to the recent net security flaw scare about network vulnerability I went on Doxpara and tested my system to see if my provider was patched and it keeps coming up with toorrr.com is trying to gain access to my ports ...is that you guys? could there be still remnants of the program on my pc after uninstall.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-189"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-189" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 17, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-148" class="permalink" rel="bookmark">uninstall problem?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-189">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-189" class="permalink" rel="bookmark">that&#039;s not us.  It&#039;s the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>that's not us.  It's the domain Kaminsky used to test your dns.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-489"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-489" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Tarpeian (not verified)</span> said:</p>
      <p class="date-time">January 03, 2009</p>
    </div>
    <a href="#comment-489">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-489" class="permalink" rel="bookmark">Torbutton uninstalled:  Cookies Still Not Accepted</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have uninstalled not just torbutton but all the related TOR suite, yet Mozilla Firefox still won't accepted needed cookies for such things as gmail.  All the correct preferences in Mozilla for cookies are checked.  Mozilla now twice reinstalled.  Even Did a p[re-TOR System Restore.  Cookies still not accepted.</p>
<p>Have switched back to IE, but would prefer Mozilla.</p>
<p>Any help appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-494"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-494" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 04, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-489" class="permalink" rel="bookmark">Torbutton uninstalled:  Cookies Still Not Accepted</a> by <span>Tarpeian (not verified)</span></p>
    <a href="#comment-494">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-494" class="permalink" rel="bookmark">check your settings</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>have you tried changing your cookie preferences?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
