title: Stop the FCC from Destroying Net Neutrality
---
pub_date: 2017-07-12
---
author: tommy
---
tags:

internet freedom
net neutrality
---
_html_body:

<p> </p>
<p>Today, we stand with more than 50,000 others in a battle for net neutrality. The FCC wants to destroy net neutrality and give big cable companies control over what we see and do online. If they get their way, they’ll allow widespread throttling, blocking, censorship, and extra fees.</p>
<h3>The internet needs to be free </h3>
<p>One of Tor’s founding principles is that free speech is a non-negotiable requirement for free societies. It’s been one of our core guidelines for more than a decade. Net neutrality ensures that the internet is truly a level playing field, a place where everyone is heard. We shouldn’t cede control of our web browsing to ISPs like AT&amp;T and Verizon, and unless we want an internet of paid tiers and “priority service,” we need to speak up now. Check out (and share!) <a href="https://vimeo.com/222706185">this great video</a> from Vimeo about why net neutrality is critical to internet freedom.</p>
<h3>Join the fight </h3>
<p>Before you do anything else, send a letter to the FCC &amp; Congress now: <a href="https://battleforthenet.com/">https://battleforthenet.com/</a></p>
<p>Next, Join the conversation on Twitter with the #NetNeutrality hashtag. Here are a couple suggestions: </p>
<ul>
<li dir="ltr">
<p dir="ltr"><b id="docs-internal-guid-8aca4862-377a-ed4f-8cfd-ac54b23d3f54">Killing #NetNeutrality will open the floodgates for Internet censorship. Tell the FCC, "we need net neutrality": BattlefortheNet.com</b></p>
</li>
<li dir="ltr">
<p dir="ltr"><b id="docs-internal-guid-8aca4862-377a-ed4f-8cfd-ac54b23d3f54">#NetNeutrality is the First Amendment of the internet. Take action now to stop Big Cable from destroying it: BattlefortheNet.com</b></p>
</li>
</ul>
<p dir="ltr">“If we lose net neutrality, the Internet will never be the same,” said Evan Greer, campaign director of Fight for the Future, one of the leading groups behind the protest. “No one wants their cable company to have control over what they can see and do online. Internet users know that their freedom is worth fighting for, and on July 12, we’ll fight to win.”</p>
<p> </p>

