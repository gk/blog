title: Tails 0.20 is out
---
pub_date: 2013-08-09
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.20, is out.</p>

<p>All users must upgrade as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.19/" rel="nofollow">numerous security issues</a>.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>New features
<ul>
<li>Install Linux kernel 3.10.3-1 from Debian unstable.</li>
<li>Iceweasel 17.0.8esr + Torbrowser patches.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Prevent Iceweasel from displaying a warning when leaving HTTPS web sites.</li>
<li>Make Iceweasel use the correct, localized search engine.</li>
<li>Fix Git access to https:// repositories.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Install Dasher, a predictive text entry tool.</li>
<li>Add a wrapper around TrueCrypt which displays a warning about it soon being deprecated in Tails.</li>
<li>Remove Pidgin libraries for all protocols but IRC and Jabber/XMPP. Many of the other protocols Pidgin support are broken in Tails and haven't got any security auditting.</li>
<li>Disable the pre-defined Pidgin accounts so they do not auto-connect on Pidgin start.</li>
<li>Include information about Alsa in WhisperBack reports.</li>
<li>Explicitly restrict access to ptrace. While this setting was enabled by default in Debian's Linux 3.9.6-1, it will later disabled in 3.9.7-1. It's unclear what will happen next, so let's explicitly enable it ourselves.</li>
<li>Do not display dialog when a message is sent in Claws Mail.</li>
<li>Sync iceweasel preferences with the Torbrowser's.</li>
</ul>
</li>
<li>Localization
<ul>
<li>Many translation updates all over the place.</li>
<li>Merge all Tails-related POT files into one, and make use of intltoolize for better integration with Transifex.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog?id=0.20" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issue</strong></p>

<p>No new known issue but <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">longstanding known issues</a>.</p>

<p><strong>I want to try it / to upgrade!</strong></p>

<p>See the <a href="https://tails.boum.org/getting_started/" rel="nofollow">Getting started</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is scheduled for around September 19.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow">you can contribute</a>. If you want to help, <a href="https://tails.boum.org/support/" rel="nofollow">come talk to us</a>!</p>

---
_comments:

<a id="comment-33403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33403" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2013</p>
    </div>
    <a href="#comment-33403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33403" class="permalink" rel="bookmark">Tails&#039; forum has been closed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tails' forum has been closed since the day the world learned about the Big Brother's surveillance program.</p>
<p>May I suggest that both Tor and Tails share a forum. The reason is whenever there is a new version of Tails, Tor will announce it at Tor's site.</p>
<p>I believe a forum has already been created for Tor users. It is at <a href="http://torforum.org" rel="nofollow">http://torforum.org</a>. Tails' users should use it for their discussion.</p>
<p>As more and more people get to use Tor, they will start to use Tails and consequently have Tails' related questions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-33419"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33419" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 10, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-33403" class="permalink" rel="bookmark">Tails&#039; forum has been closed</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-33419">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33419" class="permalink" rel="bookmark">I agree that it would be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree that it would be nice to put all the various Tor projects and users into one forum system. So far we've had difficulty finding one that all (heck, any) of the projects like.</p>
<p>The one you refer to is not an official forum for any of the projects, and likely has roughly zero competent Tor developers on it. Careful with what you find out there on the Internets!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-33440"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33440" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2013</p>
    </div>
    <a href="#comment-33440">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33440" class="permalink" rel="bookmark">The new version works</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The new version works flawlessly but I've been unable to access Tormail. Is the site down and is anyone else having issues as well?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-33448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-33448" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 10, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-33440" class="permalink" rel="bookmark">The new version works</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-33448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-33448" class="permalink" rel="bookmark">You might want to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You might want to read<br />
<a href="https://blog.torproject.org/blog/tor-security-advisory-old-tor-browser-bundles-vulnerable" rel="nofollow">https://blog.torproject.org/blog/tor-security-advisory-old-tor-browser-…</a><br />
including some of the comments.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
