title: Tor Weekly News — September 10th, 2014
---
pub_date: 2014-09-10
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-sixth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>More monthly status reports for August 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of August continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000643.html" rel="nofollow">Yawning Angel</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000644.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000646.html" rel="nofollow">Isis Lovecruft</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000647.html" rel="nofollow">Colin C.</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000648.html" rel="nofollow">Griffin Boyce</a>.</p>

<p>Arturo Filastò reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000645.html" rel="nofollow">OONI team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-September/003752.html" rel="nofollow">announced</a> the release of Orbot 14.0.8, containing “some fixes for people who like to fiddle with transproxy/iptables settings, which can lead to the device getting into a bad network state”, as well as for “a common freak crash that was occuring on app exit in some cases.” See Nathan’s message for a full changelog and download links.</p>

<p>Mike Perry <a href="https://lists.torproject.org/pipermail/tor-talk/2014-September/034606.html" rel="nofollow">asked for comments</a> on his proposal to drop Tor Browser support for Mac OS X 10.6, which is no longer receiving security updates from Apple. This means that the Tor Browser team would only have to distribute standard-sized 64-bit builds for Mac OS X rather than the oversized 32+64-bit set. Users who are unable to upgrade their operating system would still be able to use Tails for their Tor browsing needs.</p>

<p>Hartmut Haase <a href="https://lists.torproject.org/pipermail/tor-talk/2014-September/034666.html" rel="nofollow">reported</a> that Tor Browser occasionally fails to open, despite a successful connection being made to the Tor network; several other users confirmed that they are also experiencing this problem. Georg Koppen <a href="https://lists.torproject.org/pipermail/tor-talk/2014-September/034678.html" rel="nofollow">suggested</a> that the issue is the one covered by <a href="https://bugs.torproject.org/10804" rel="nofollow">bug ticket #10804</a>: “Solving this is high on the priority list, but alas not as high as getting everything ready for the switch to ESR31.”</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-September/000681.html" rel="nofollow">Peter Ludikovsky</a> and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-September/000685.html" rel="nofollow">goll</a> for running mirrors of the Tor Project website and software archive!</p>

<p>Andrew Lewman <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-September/000690.html" rel="nofollow">published</a> the results of a test he ran to answer the question “Why not just use CloudFlare for mirrors of the Tor Project website?”: “The results are that using CloudFlare doesn’t offload the binaries, which are what make up the bulk of traffic on the mirror […] I’ve started to look at CDN providers to see if there are affordable services which can offload the entire site itself.”</p>

<p>As part of an ongoing effort to rescue the Tor blog from rot and ruin caused by broken Drupal code, ultrasandwich <a href="https://bugs.torproject.org/10022#comment:22" rel="nofollow">set up</a> an <a href="http://tor-blog.deadhare.com/" rel="nofollow">unofficial preview</a> of a possible blog based on the Jekyll static site generator. If you want to contribute to the revamp of the Tor Project website, including the blog, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/www-team" rel="nofollow">www-team mailing list</a> awaits your comments and ideas!</p>

<h1>Tor help desk roundup</h1>

<p>Users want to know if their personal information is safe when they use Tor Browser. Personal accounts are no less secure using Tor Browser than they are using the web without Tor: the problem of authenticating websites and preventing eavesdropping has been addressed outside of the Tor context through HTTPS. That’s why the Tor Browser ships with the <a href="https://www.eff.org/https-everywhere" rel="nofollow">HTTPS-Everywhere browser extension</a> — for every website you visit, HTTPS-Everywhere checks  whether or not that website is known to have an HTTPS version, and if so  it connects to the site using HTTPS instead of HTTP. Tor + HTTPS provides  full end-to-end encryption when visiting any site that offers its content  via HTTPS. Using HTTPS with Tor helps keep users’ web accounts secure.</p>

<h1>Easy development tasks to get involved with</h1>

<p>If a single human or organization runs more than one relay, they should configure all their relays to be in the same “family”, the goal being to prevent clients from using more than one of these relays in the same circuit. However, the config option used for this, MyFamily, only accepts relay fingerprints that are preceeded by $, unlike most other config options. It would be great if this option accepted fingerprints preceeded by $, as well as without it. Nick Mathewson says this ticket would be pretty easy, so why not give it a try? It does sound like some fun C hacking. Be sure to post your patch to the <a href="https://bugs.torproject.org/12093" rel="nofollow">ticket</a>.</p>

<p>Back in the day, the tor daemon, which is the core of the Tor network, compiled and ran on Windows 98. But that’s history, and aren’t we all glad? Somebody should identify and drop support code for all Windows versions prior to Windows XP. Nick says “this is mainly going to be a matter of identifying cases where we use LoadLibrary and GetProcAddress to find always-present-functions in always-present DLLs.” If the previous sentence made any sense to you, maybe you’re a good person to help with this! Be sure to comment on the <a href="https://bugs.torproject.org/11444" rel="nofollow">ticket</a> if you have a branch to review.</p>

<p>This issue of Tor Weekly News has been assembled by harmony, Matt Pagan, Karsten Loesing, and Lunar.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

