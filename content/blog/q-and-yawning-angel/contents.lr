title: Q and A with Yawning Angel
---
pub_date: 2016-10-11
---
author: ailanthus
---
tags:

tor browser
firefox
mozilla
sandbox
yawning angel
---
categories:

applications
partners
---
_html_body:

<p><cite>Here's an interview I just did with our own Yawning Angel, a longtime Tor developer, about his work on a Linux prototype for a sandbox for the Tor Browser.</cite></p>

<p><strong>What is a sandbox?</strong></p>

<p>It’s a separate environment from the rest of your computer where you run untrusted programs. We’re running Tor Browser.</p>

<p>The idea is that exploits targeting Tor Browser are trapped inside the sandbox and can’t get out and mess with the rest of your computer or deanonymize you.</p>

<p>The amount of information Tor Browser will learn about your computer, and thereby you, will be limited. For example, the sandbox will hide things like your files, and real IP and MAC addresses from Tor Browser.</p>

<p>Tor Browser can only access or manipulate the insides of the sandbox. It's like Plato's Allegory of the Cave.  The only reality Tor Browser knows is the inside of the sandbox (cave).  We prevent it from interacting with the rest of your computer (the outside world), except via the Tor Network (shadows on the wall).</p>

<p><strong>How will the sandbox help users?</strong></p>

<p>It should make Tor a lot safer for users. We know there are people who try to de-anonymize Tor users by exploiting Firefox. Having Tor Browser run in a sandbox makes their life a lot harder.</p>

<p><strong>Which operating system will the sandbox support?</strong></p>

<p>We need a sandbox for Linux, OSX, and Windows. I’m working on the Linux one. The Tor browser team is looking at OSX.  In the future we’d like to do Windows.</p>

<p><strong>Can you talk about the sandbox a bit more?</strong></p>

<p>I use a Go application to manage installing and updating Tor Browser, and set up the sandbox using a utility called <a href="https://github.com/projectatomic/bubblewrap" rel="nofollow">bubblewrap</a> (the underlying sandboxing code also used by <a href="http://flatpak.org" rel="nofollow">Flatpak</a>) which is based around Linux's container support.</p>

<p>It ended up being something superficially similar to what the <a href="https://subgraph.com/" rel="nofollow">Subgraph OS</a> project has done, but my approach is more targeted as "something you can just download and start using on your existing Linux system", and theirs, as far as I am aware, is more oriented around being a full OS replacement.</p>

<p><strong>Why are you doing this?</strong></p>

<p>It's an interesting technical challenge, and in the light of recent events  like <a href="https://blog.torproject.org/blog/fbis-quiet-plan-begin-mass-hacking" rel="nofollow">The FBI’s Quiet Plan to Begin Mass Hacking</a>, defending users against malicious attackers at the application layer is incredibly important.</p>

<p><strong>Why did we not have this before?</strong></p>

<p>Developer time—we have a lot that we already need to do. We never have time to do this. We have a funding proposal to do this but I decided to do it separately from the Tor Browser team. I’ve been trying to do this since last year. This is my third attempt. I failed twice at coming up with something that I like, but the third time appears to be the charm.</p>

<p><strong>What was the hardest part?</strong></p>

<p>Lots of design problems. It’s incredibly complicated.</p>

<p><strong>What else have you worked on?</strong> </p>

<p>Everything—I’ve touched a lot of our code. I designed and wrote obfs4, Meek on Android uses my code, and I work on core Tor.</p>

<p><strong>When will the sandbox be available to users?</strong></p>

<p>This is experimental.  Right now I have something that  works on <cite>my</cite> laptop. It is not user friendly at all. It’s a functional prototype. By the end of the year it will be available in alpha form for early adopters to experiment with.</p>

<p><strong>What are you working on right now?</strong> </p>

<p>There’re a few security versus usability tradeoffs. Most users will disagree with the tradeoffs I’ve made for myself, so I have to make all that configurable. For example, do we want to give the sandbox access the sound card? We will make it user configurable.</p>

<p><strong>Mozilla is also working on something like this, right?</strong></p>

<p>Mozilla is working primarily on efforts to sandbox the <a href="https://wiki.mozilla.org/Security/Sandbox" rel="nofollow">content, media and plugin processes</a> (roughly a per-tab sandbox).<br />
In our version, the entire browser is running in a sandbox. </p>

<p>Both projects in the long run should work to complement each other, since both are a good idea.</p>

