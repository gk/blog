title: Tor 0.2.6.5-rc is released
---
pub_date: 2015-03-18
---
author: nickm
---
_html_body:

<p>Tor 0.2.6.5-rc is the second and (hopefully) last release candidate in the 0.2.6. It fixes a small number of bugs found in 0.2.6.4-rc. It is the smallest 0.2.6 release to date, but has a couple of important fixes.</p>

<p>You can download the source from the website; I'd hope that the releveant packages will be online before long, and that this will ship with the next TorBrowser.</p>

<p>If you're curious about all the cool features coming up in Tor 0.2.6, <a href="https://blog.torproject.org/blog/coming-tor-026" rel="nofollow">I wrote a post about it a couple of weeks ago</a>.</p>

<h2>Changes in version 0.2.6.5-rc - 2015-03-18</h2>

<ul>
<li>Major bugfixes (client):
<ul>
<li>Avoid crashing when making certain configuration option changes on clients. Fixes bug 15245; bugfix on 0.2.6.3-alpha. Reported by "anonym".
  </li>
</ul>
</li>
<li>Major bugfixes (pluggable transports):
<ul>
<li>Initialize the extended OR Port authentication cookie before launching pluggable transports. This prevents a race condition that occured when server-side pluggable transports would cache the authentication cookie before it has been (re)generated. Fixes bug 15240; bugfix on 0.2.5.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (portability):
<ul>
<li>Do not crash on startup when running on Solaris. Fixes a bug related to our fix for 9495; bugfix on 0.2.6.1-alpha. Reported by "ruebezahl".
  </li>
</ul>
</li>
<li>Minor features (heartbeat):
<ul>
<li>On relays, report how many connections we negotiated using each version of the Tor link protocols. This information will let us know if removing support for very old versions of the Tor protocols is harming the network. Closes ticket 15212.
  </li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Refactor main loop to extract the 'loop' part. This makes it easier to run Tor under Shadow. Closes ticket 15176.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-90248"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90248" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 19, 2015</p>
    </div>
    <a href="#comment-90248">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90248" class="permalink" rel="bookmark">i cant download it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i cant download it</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-90442"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90442" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2015</p>
    </div>
    <a href="#comment-90442">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90442" class="permalink" rel="bookmark">New security fixes for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New security fixes for Firefox ESR <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2015-29/" rel="nofollow">https://www.mozilla.org/en-US/security/advisories/mfsa2015-29/</a> <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2015-28/" rel="nofollow">https://www.mozilla.org/en-US/security/advisories/mfsa2015-28/</a><br />
Are you going to release an update?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-90464"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90464" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2015</p>
    </div>
    <a href="#comment-90464">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90464" class="permalink" rel="bookmark">What prevents Tor from using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What prevents Tor from using NSA-owned or -hacked nodes?<br />
What if NSA set up a ton of nodes in virtual machines on a supercomputer?<br />
Why should we be confident that Tor is anything more than a mere annoyance to NSA?<br />
someone please explain.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-91109"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-91109" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-90464" class="permalink" rel="bookmark">What prevents Tor from using</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-91109">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-91109" class="permalink" rel="bookmark">NSA is your friend, Google</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NSA is your friend, Google is your friend. Tell me who are your friends and I say who you are.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-90596"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90596" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2015</p>
    </div>
    <a href="#comment-90596">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90596" class="permalink" rel="bookmark">TOR v. 4.0.5 cannot</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TOR v. 4.0.5 cannot establish a connection.TOR v. 3.6.5 does.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-90640"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90640" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2015</p>
    </div>
    <a href="#comment-90640">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90640" class="permalink" rel="bookmark">Is there any documentation</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there any documentation how to start Tor in Windows without Vidalia or TorBrowser?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-90641"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90641" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2015</p>
    </div>
    <a href="#comment-90641">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90641" class="permalink" rel="bookmark">Also &#039;Expert Bundle&#039; link</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Also 'Expert Bundle' link from 'Downloads' page is broken: <a href="https://dist.torproject.org/torbrowser/4.0.5/tor-win32-tor-0.2.5.10.zip" rel="nofollow">https://dist.torproject.org/torbrowser/4.0.5/tor-win32-tor-0.2.5.10.zip</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-90856"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-90856" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 26, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-90641" class="permalink" rel="bookmark">Also &#039;Expert Bundle&#039; link</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-90856">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-90856" class="permalink" rel="bookmark">Thanks, fixed.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, fixed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
