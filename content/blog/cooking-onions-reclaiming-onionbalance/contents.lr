title: Cooking with Onions: Reclaiming the Onionbalance
---
pub_date: 2020-03-17
---
author: asn
---
tags:

onion services
cooking with onions
onionbalance
---
categories:

applications
onion services
---
summary: Onionbalance is one of the standard ways onion service administrators can load balance onion services, but it didn't work for v3 onions. Until now.
---
_html_body:

<div class="ace-line" id="magicdomid2"><span class="author-a-z90zlz85zz75z7nz78zz122z88r22z85zz87zq">Welcome to another issue of <em><a href="https://blog.torproject.org/category/tags/cooking-onions">Cooking with Onions</a></em>!</span></div>
<div class="ace-line" id="magicdomid11"> </div>
<div class="ace-line" id="magicdomid12"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://blog.torproject.org/cooking-onions-finding-onionbalance">Onionbalance</a> is one of the standard ways onion service administrators can load balance onion services, but it didn't work for v3 onions. Until now</span><span class="author-a-z90zlz85zz75z7nz78zz122z88r22z85zz87zq">.</span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"> </span><strong><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">We just <a href="https://gitlab.torproject.org/asn/onionbalance">released a new version</a> of Onionbalance that supports v3 onion services.</span></strong></div>
<div class="ace-line"> </div>
<div class="ace-line"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">The core functionality remains the same: Onionbalance allows onion service operators to achieve the property of </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z i"><i>high availability</i></span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"> by allowing multiple machines to handle requests for </span><span class="author-a-z90zlz85zz75z7nz78zz122z88r22z85zz87zq">an on</span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">ion service.</span></div>
<div class="ace-line" id="magicdomid15"> </div>
<div class="ace-line" id="magicdomid17"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">If you are already familiar with configuring Tor onion services, setting up Onionbalance is simple. Check the precise setup instructions on our <a href="https://onionbalance-v3.readthedocs.io/en/latest/v3/tutorial-v3.html#tutorial-v3">documentation page</a>. </span></div>
<h2 class="ace-line" id="magicdomid18"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">What took you so long?</span></h2>
<div class="ace-line" id="magicdomid21"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">We deployed </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://blog.torproject.org/tors-fall-harvest-next-generation-onion-services">next generation v3 onions</a> two years ago, but we just added v3 support for Onionbalance. This took a while...</span></div>
<div class="ace-line" id="magicdomid22"> </div>
<div class="ace-line" id="magicdomid23"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Introducing v3 functionality to Onionbalance wasn't easy. The v3 protocol is more secure and robust than v2, but those benefits come with a more complicated protocol. That made Onionbalance integration harder.</span></div>
<div class="ace-line" id="magicdomid24"> </div>
<div class="ace-line" id="magicdomid26"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">To implement v3 support for Onionbalance, we had to </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">jump <a href="https://trac.torproject.org/projects/tor/ticket/29583">through various</a></span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://trac.torproject.org/projects/tor/ticket/29583"> hoops</a>, and in some cases, we had to take the more complicated scenic route.</span></div>
<div class="ace-line" id="magicdomid27"> </div>
<div class="ace-line" id="magicdomid29"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">In particular, </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">some of the</span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"> <a href="https://trac.torproject.org/projects/tor/ticket/32709">advanced v3 crypto</a> was not allowing us to pull the classic Onionbalance trick of making a user think they are going to the frontend service but in reality visiting a backend service. To work around this, we were forced to make the OnionBalance setup procedure a bit more complicated: </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z b"><b>operators now need to explicitly configure their backend services to act as Onionbalance instances via the torrc.</b></span></div>
<div class="ace-line" id="magicdomid30"> </div>
<div class="ace-line" id="magicdomid32"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Finally, as part of our work on Onionbalance v3, we also had to implement<a href="https://stem.torproject.org/api/descriptor/hidden_service.html#stem.descriptor.hidden_service.HiddenServiceDescriptorV3"> </a></span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://stem.torproject.org/api/descriptor/hidden_service.html#stem.descriptor.hidden_service.HiddenServiceDescriptorV3">v3 descriptor support </a>for </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://stem.torproject.org">Stem</a>, a Python controller library for Tor.</span></div>
<h2 class="ace-line" id="magicdomid34"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">What's next?</span></h2>
<div class="ace-line" id="magicdomid36"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">We're still missing Onionbalance packages for Debian and pip. We want to conduct more tests before replacing the existing packages with the new Onionbalance, so we expect these to be ready in April. The current version of Onionbalance is 0.1.9, and our plan is to create the packages when 0.2.0 is released.</span></div>
<div class="ace-line" id="magicdomid37"> </div>
<div class="ace-line" id="magicdomid38"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Additionally, these features are still to come: </span></div>
<ul>
<li class="ace-line" id="magicdomid40"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Support for v3 </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://onionbalance-v3.readthedocs.io/en/latest/v2/design.html#choice-of-introduction-points">"distinct descriptor" mode</a>. This mode allows Onionbalance v2 to load-balance more than 10 backend instances, whereas currently Onionbalance v3 has a limit of 8 backend instances. In theory, Onionbalance could load-balance hundreds of backend instances by publishing descriptors at small time intervals that contain introduction points from a different subset of those instances each time.</span></li>
<li class="ace-line"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Minimize the differences between both v3 and other descriptors. Currently Onionbalance v3 descriptors can look different from other descriptors, which makes it possible for clients and HSDirs to learn that a service is using Onionbalance. This can be an issue for more </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z"><a href="https://github.com/mikeperry-tor/vanguards/blob/master/README_SECURITY.md#how-to-onionbalance">advanced onion service threat models</a>. </span></li>
<li class="ace-line"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Enable client authorization on the frontend service. This may be needed in specialized use cases. Adding this feature would first require implementing client authorization support to Stem v3 descriptors and then using that feature in Onionbalance.</span></li>
<li class="ace-line"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Allow the ability to transfer your existing v3 onion service to Onionbalance.</span></li>
</ul>
<div class="ace-line" id="magicdomid49"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">Right now, any sort of testing and feedback would be really helpful for us. Also, if you are in the mood for coding or fixing bugs, it would be great if you could provide patches for any of the above missing features or any other feature you might need: We also </span><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">have a <a href="https://github.com/asn-d6/onionbalance">GitHub mirror</a> for people who feel more comfortable there.</span></div>
<div class="ace-line"> </div>
<div class="ace-line"><span class="author-a-z71zmz77zz78zg957z72zz87z6z68zz79zjz79zz90z">We hope this new flavor gives the transition to v3 onion services a boost. And now that Onionbalance is back in active development, maybe you'll join us in kitchen and write some code? </span></div>
<div class="ace-line" id="magicdomid52"> </div>
<div class="ace-line" id="magicdomid53"><span class="author-a-z90zlz85zz75z7nz78zz122z88r22z85zz87zq">Until next time! :)</span></div>

---
_comments:

<a id="comment-287085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287085" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2020</p>
    </div>
    <a href="#comment-287085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287085" class="permalink" rel="bookmark">Can you stop using github…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you stop using github and host them internally on gitea? Github's account creation is blocking tor.</p>
<p>gitea.torproject.org will be nice</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287089"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287089" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">March 18, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287085" class="permalink" rel="bookmark">Can you stop using github…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287089">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287089" class="permalink" rel="bookmark">Hello my friend,
the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello my friend,</p>
<p>the official repo of onionbalance is now on Tor project's gitlab self-hosted server: <a href="https://gitlab.torproject.org/asn/onionbalance" rel="nofollow">https://gitlab.torproject.org/asn/onionbalance</a></p>
<p>We have a github mirror for people who feel more comfortable there. It's completely optional and you can ignore it safely.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287086"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287086" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2020</p>
    </div>
    <a href="#comment-287086">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287086" class="permalink" rel="bookmark">I don&#039;t run an onion, but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't run an onion, but this is cool. Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287090"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287090" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2020</p>
    </div>
    <a href="#comment-287090">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287090" class="permalink" rel="bookmark">Hello,
I agree that GitLab…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,<br />
I agree that GitLab ia better than GitHub, but I think there are better alternatives.</p>
<p>*Sourcehut.org* e.g. is Java-Script-free, Free Software and hostable</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287188" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2020</p>
    </div>
    <a href="#comment-287188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287188" class="permalink" rel="bookmark">I really don&#039;t know anything…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I really don't know anything about Onion Balance or the Tor HS internals, but I can't help but think... wouldn't it have been smarter to embed the OnionBalance logic into Tor directly when v3 was being developed? Such that any two or more Tor instances hosting an HS with the same private key would automatically share requests? This is how i2p does multihoming, although i2p has the convenience of being DHT-based. Just wondering.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287191"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287191" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">March 24, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287188" class="permalink" rel="bookmark">I really don&#039;t know anything…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287191">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287191" class="permalink" rel="bookmark">Hello friend,
if you have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello friend,</p>
<p>if you have multiple instances hosting the same HS private key, they will automatically share requests, but there are a bunch of drawbacks to that approach VS onionbalance. Check this out: <a href="https://trac.torproject.org/projects/tor/ticket/26768#comment:3" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/26768#comment:3</a></p>
<p>Thanks for your comment!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287422"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287422" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ajay (not verified)</span> said:</p>
      <p class="date-time">April 05, 2020</p>
    </div>
    <a href="#comment-287422">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287422" class="permalink" rel="bookmark">Nice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote></blockquote>
<p>Nice</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287920"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287920" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Old Bill (not verified)</span> said:</p>
      <p class="date-time">May 22, 2020</p>
    </div>
    <a href="#comment-287920">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287920" class="permalink" rel="bookmark">Why is there a limit of 8…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is there a limit of 8 backends for v3?</p>
</div>
  </div>
</article>
<!-- Comment END -->
