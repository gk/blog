title: New Release: Tor Browser 10.5a9 (Android Only)
---
pub_date: 2021-02-07
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a9 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a9/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable release for <a href="https://blog.torproject.org/new-release-tor-browser-1005">Android</a> instead.</p>
<p>This release updates Fenix to 86.0.0-beta.2. Additionally, we update NoScript to 11.2 and HTTPS Everywhere to 2021.1.27.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a8 is:</p>
<ul>
<li>Android
<ul>
<li>Update Fenix to 86.0.0-beta.2</li>
<li>Update HTTPS Everywhere to 2021.1.27</li>
<li>Update NoScript to 11.2</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40041">Bug 40041</a>: Rebase android-components patches for Fenix 86 beta 2 builds</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40109">Bug 40109</a>: Reduce requested permissions</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40141">Bug 40141</a>: Hide EME site permission</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40143">Bug 40143</a>: Use deterministic date in Test apk</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40146">Bug 40146</a>: Rebase Fenix patches to Fenix 86 beta 2</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40188">Bug 40188</a>: Build and ship snowflake only if it is enabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40212">Bug 40212</a>: Bump version of snowflake and webrtc</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40308">Bug 40308</a>: Disable network partitioning until we evaluate dFPI</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40309">Bug 40309</a>: Avoid using regional OS locales</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40320">Bug 40320</a>: Rebase tor-browser patches to 86.0b5</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40214">Bug 40214</a>: Update AMO Collection URL</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40217">Bug 40217</a>: Update components for switch to mozilla86-based Fenix</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291089"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291089" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
    <a href="#comment-291089">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291089" class="permalink" rel="bookmark">GitLab pages are not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>GitLab pages are not readable when JavaScript is disabled in the browser. They don't have to work 100% but at least basic content of the issues should be visible.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291091"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291091" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 08, 2021</p>
    </div>
    <a href="#comment-291091">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291091" class="permalink" rel="bookmark">Bug 40109: Reduce requested…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p><a href="https://bugs.torproject.org/tpo/applications/fenix/40109" rel="nofollow">Bug 40109</a>: Reduce requested permissions</p></blockquote>
<p>Eagerly waiting for this in the standard release! Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonumoyse (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291102" class="permalink" rel="bookmark">Hi! Tor Browser for Android…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi! Tor Browser for Android 10.5a9, noticed that after closing the browser, information accumulates in the cache storage. Why is this happening? Is this normal? Is this how it should be?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291103"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291103" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>rrrrmmngop (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291103">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291103" class="permalink" rel="bookmark">Location option is stuck at…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Location option is stuck at ask to allow<br />
Why it can't be blocked.....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291175"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291175" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291103" class="permalink" rel="bookmark">Location option is stuck at…</a> by <span>rrrrmmngop (not verified)</span></p>
    <a href="#comment-291175">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291175" class="permalink" rel="bookmark">Thanks for reporting this!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for reporting this! That's a bug.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291156"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291156" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2021</p>
    </div>
    <a href="#comment-291156">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291156" class="permalink" rel="bookmark">Thanks for finally removing…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for finally removing the needless permissions and trackers.... oh wait.... you didn't bother... again</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291160"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291160" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2021</p>
    </div>
    <a href="#comment-291160">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291160" class="permalink" rel="bookmark">Thanks for the update. For…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the update. For TB 10.5a9, I'm still seeing new Fenix's non-deniable permissions that are being requested in the Play Store:</p>
<ul>
<li>download files without notification (*)</li>
<li>Run at startup (*)</li>
<li>use biometric hardware (*)</li>
<li>Play Install Referrer API</li>
<li>View Wi-Fi connections (*)</li>
<li>use fingerprint hardware (*)</li>
<li>receive data from Internet</li>
</ul>
<p>Those that I marked with (*) are privacy-related and should not be requested.</p>
</div>
  </div>
</article>
<!-- Comment END -->
