title: Tor 0.3.4.2-alpha is released!
---
pub_date: 2018-06-12
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>Hi!  There's a new alpha release available for download.  If you build Tor from source, you can download the source code for 0.3.4.2-alpha from the usual place on the website.  Packages should be available over the coming weeks, with a new alpha Tor Browser release over the coming weeks.</p>
<p>Tor 0.3.4.2-alpha fixes several minor bugs in the previous alpha release, and forward-ports an authority-only security fix from 0.3.3.6.</p>
<h2>Changes in version 0.3.4.2-alpha - 2018-06-12</h2>
<ul>
<li>Directory authority changes:
<ul>
<li>Add an IPv6 address for the "dannenberg" directory authority. Closes ticket <a href="https://bugs.torproject.org/26343">26343</a>.</li>
</ul>
</li>
<li>Major bugfixes (security, directory authority, denial-of-service, also in 0.3.3.6):
<ul>
<li>Fix a bug that could have allowed an attacker to force a directory authority to use up all its RAM by passing it a maliciously crafted protocol versions string. Fixes bug <a href="https://bugs.torproject.org/25517">25517</a>; bugfix on 0.2.9.4-alpha. This issue is also tracked as TROVE-2018-005.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (continuous integration):
<ul>
<li>Add the necessary configuration files for continuous integration testing on Windows, via the Appveyor platform. Closes ticket <a href="https://bugs.torproject.org/25549">25549</a>. Patches from Marcin Cieślak and Isis Lovecruft.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the June 7 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/26351">26351</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compatibility, openssl):
<ul>
<li>Work around a change in OpenSSL 1.1.1 where return values that would previously indicate "no password" now indicate an empty password. Without this workaround, Tor instances running with OpenSSL 1.1.1 would accept descriptors that other Tor instances would reject. Fixes bug <a href="https://bugs.torproject.org/26116">26116</a>; bugfix on 0.2.5.16.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Silence unused-const-variable warnings in zstd.h with some GCC versions. Fixes bug <a href="https://bugs.torproject.org/26272">26272</a>; bugfix on 0.3.1.1-alpha.</li>
<li>Fix compilation when using OpenSSL 1.1.0 with the "no-deprecated" flag enabled. Fixes bug <a href="https://bugs.torproject.org/26156">26156</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Avoid a compiler warning when casting the return value of smartlist_len() to double with DEBUG_SMARTLIST enabled. Fixes bug <a href="https://bugs.torproject.org/26283">26283</a>; bugfix on 0.2.4.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control port):
<ul>
<li>Do not count 0-length RELAY_COMMAND_DATA cells as valid data in CIRC_BW events. Previously, such cells were counted entirely in the OVERHEAD field. Now they are not. Fixes bug <a href="https://bugs.torproject.org/26259">26259</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller):
<ul>
<li>Improve accuracy of the BUILDTIMEOUT_SET control port event's TIMEOUT_RATE and CLOSE_RATE fields. (We were previously miscounting the total number of circuits for these field values.) Fixes bug <a href="https://bugs.torproject.org/26121">26121</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (hardening):
<ul>
<li>Prevent a possible out-of-bounds smartlist read in protover_compute_vote(). Fixes bug <a href="https://bugs.torproject.org/26196">26196</a>; bugfix on 0.2.9.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Fix a bug that blocked the creation of ephemeral v3 onion services. Fixes bug <a href="https://bugs.torproject.org/25939">25939</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (test coverage tools):
<ul>
<li>Update our "cov-diff" script to handle output from the latest version of gcov, and to remove extraneous timestamp information from its output. Fixes bugs 26101 and 26102; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
</ul>

