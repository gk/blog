title: New alpha release: Tor 0.4.2.2-alpha
---
pub_date: 2019-10-07
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.2.2-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely in the next couple of weeks.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>This release fixes several bugs from the previous alpha release, and from earlier versions. It also includes a change in authorities, so that they begin to reject the currently unsupported release series.</p>
<h2>Changes in version 0.4.2.2-alpha - 2019-10-07</h2>
<ul>
<li>Major features (directory authorities):
<ul>
<li>Directory authorities now reject relays running all currently deprecated release series. The currently supported release series are: 0.2.9, 0.3.5, 0.4.0, 0.4.1, and 0.4.2. Closes ticket <a href="https://bugs.torproject.org/31549">31549</a>.</li>
</ul>
</li>
<li>Major bugfixes (embedded Tor):
<ul>
<li>Avoid a possible crash when restarting Tor in embedded mode and enabling a different set of publish/subscribe messages. Fixes bug <a href="https://bugs.torproject.org/31898">31898</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (torrc parsing):
<ul>
<li>Stop ignoring torrc options after an %include directive, when the included directory ends with a file that does not contain any config options (but does contain comments or whitespace). Fixes bug <a href="https://bugs.torproject.org/31408">31408</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor features (auto-formatting scripts):
<ul>
<li>When annotating C macros, never generate a line that our check- spaces script would reject. Closes ticket <a href="https://bugs.torproject.org/31759">31759</a>.</li>
<li>When annotating C macros, try to remove cases of double-negation. Closes ticket <a href="https://bugs.torproject.org/31779">31779</a>.</li>
</ul>
</li>
<li>Minor features (continuous integration):
<ul>
<li>When building on Appveyor and Travis, pass the "-k" flag to make, so that we are informed of all compilation failures, not just the first one or two. Closes ticket <a href="https://bugs.torproject.org/31372">31372</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the October 1 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/31931">31931</a>.</li>
</ul>
</li>
<li>Minor features (maintenance scripts):
<ul>
<li>Add a Coccinelle script to detect bugs caused by incrementing or decrementing a variable inside a call to log_debug(). Since log_debug() is a macro whose arguments are conditionally evaluated, it is usually an error to do this. One such bug was 30628, in which SENDME cells were miscounted by a decrement operator inside a log_debug() call. Closes ticket <a href="https://bugs.torproject.org/30743">30743</a>.</li>
</ul>
</li>
<li>Minor features (onion services v3):
<ul>
<li>Assist users who try to setup v2 client authorization in v3 onion services by pointing them to the right documentation. Closes ticket <a href="https://bugs.torproject.org/28966">28966</a>.</li>
</ul>
</li>
<li>Minor bugfixes (Appveyor continuous integration):
<ul>
<li>Avoid spurious errors when Appveyor CI fails before the install step. Fixes bug <a href="https://bugs.torproject.org/31884">31884</a>; bugfix on 0.3.4.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (best practices tracker):
<ul>
<li>When listing overbroad exceptions, do not also list problems, and do not list insufficiently broad exceptions. Fixes bug <a href="https://bugs.torproject.org/31338">31338</a>; bugfix on 0.4.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller protocol):
<ul>
<li>Fix the MAPADDRESS controller command to accept one or more arguments. Previously, it required two or more arguments, and ignored the first. Fixes bug <a href="https://bugs.torproject.org/31772">31772</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Add a missing check for HAVE_PTHREAD_H, because the backtrace code uses mutexes. Fixes bug <a href="https://bugs.torproject.org/31614">31614</a>; bugfix on 0.2.5.2-alpha.</li>
<li>Disable backtrace signal handlers when shutting down tor. Fixes bug <a href="https://bugs.torproject.org/31614">31614</a>; bugfix on 0.2.5.2-alpha.</li>
<li>Rate-limit our the logging message about the obsolete .exit notation. Previously, there was no limit on this warning, which could potentially be triggered many times by a hostile website. Fixes bug <a href="https://bugs.torproject.org/31466">31466</a>; bugfix on 0.2.2.1-alpha.</li>
<li>When initialising log domain masks, only set known log domains. Fixes bug <a href="https://bugs.torproject.org/31854">31854</a>; bugfix on 0.2.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, protocol violations):
<ul>
<li>Do not log a nonfatal assertion failure when receiving a VERSIONS cell on a connection using the obsolete v1 link protocol. Log a protocol_warn instead. Fixes bug <a href="https://bugs.torproject.org/31107">31107</a>; bugfix on 0.2.4.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (modules):
<ul>
<li>Explain what the optional Directory Authority module is, and what happens when it is disabled. Fixes bug <a href="https://bugs.torproject.org/31825">31825</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (multithreading):
<ul>
<li>Avoid some undefined behaviour when freeing mutexes. Fixes bug <a href="https://bugs.torproject.org/31736">31736</a>; bugfix on 0.0.7.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Avoid crashing when starting with a corrupt keys directory where the old ntor key and the new ntor key are identical. Fixes bug <a href="https://bugs.torproject.org/30916">30916</a>; bugfix on 0.2.4.8-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (tests, SunOS):
<ul>
<li>Avoid a map_anon_nofork test failure due to a signed/unsigned integer comparison. Fixes bug <a href="https://bugs.torproject.org/31897">31897</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Refactor connection_control_process_inbuf() to reduce the size of a practracker exception. Closes ticket <a href="https://bugs.torproject.org/31840">31840</a>.</li>
<li>Refactor the microdescs_parse_from_string() function into smaller pieces, for better comprehensibility. Closes ticket <a href="https://bugs.torproject.org/31675">31675</a>.</li>
<li>Use SEVERITY_MASK_IDX() to find the LOG_* mask indexes in the unit tests and fuzzers, rather than using hard-coded values. Closes ticket <a href="https://bugs.torproject.org/31334">31334</a>.</li>
<li>Interface for function `decrypt_desc_layer` cleaned up. Closes ticket <a href="https://bugs.torproject.org/31589">31589</a>.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Document the signal-safe logging behaviour in the tor man page. Also add some comments to the relevant functions. Closes ticket <a href="https://bugs.torproject.org/31839">31839</a>.</li>
<li>Explain why we can't destroy the backtrace buffer mutex. Explain why we don't need to destroy the log mutex. Closes ticket <a href="https://bugs.torproject.org/31736">31736</a>.</li>
<li>The Tor source code repository now includes a (somewhat dated) description of Tor's modular architecture, in doc/HACKING/design. This is based on the old "tor-guts.git" repository, which we are adopting and superseding. Closes ticket <a href="https://bugs.torproject.org/31849">31849</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-284411"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284411" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>john (not verified)</span> said:</p>
      <p class="date-time">October 13, 2019</p>
    </div>
    <a href="#comment-284411">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284411" class="permalink" rel="bookmark">How is your progress against…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How is your progress against fixing the ddos issue within the tor onion services? Wasn't you guys supposed to push the fix in the Tor protocol 0.4.2 release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284415"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284415" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2019</p>
    </div>
    <a href="#comment-284415">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284415" class="permalink" rel="bookmark">&gt; Directory authorities now…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Directory authorities now reject relays running all currently deprecated release series.<br />
<a href="https://metrics.torproject.org/rs.html#aggregate/version" rel="nofollow">https://metrics.torproject.org/rs.html#aggregate/version</a><br />
seems not all?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284459" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">October 16, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284415" class="permalink" rel="bookmark">&gt; Directory authorities now…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284459" class="permalink" rel="bookmark">The code to reject these…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The code to reject these versions is in 0.4.2.2-alpha, but not all authorities are running that version yet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
