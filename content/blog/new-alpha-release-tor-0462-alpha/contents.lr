title: New Alpha Release: Tor 0.4.6.2-alpha
---
pub_date: 2021-04-15
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.6.2-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely some time next week.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.6.2-alpha is the second alpha in its series. It fixes several small bugs in previous releases, and solves other issues that had enabled denial-of-service attacks and affected integration with other tools.</p>
<h2>Changes in version 0.4.6.2-alpha - 2021-04-15</h2>
<ul>
<li>Minor features (client):
<ul>
<li>Clients now check whether their streams are attempting to re-enter the Tor network (i.e. to send Tor traffic over Tor), and close them preemptively if they think exit relays will refuse them for this reason. See ticket <a href="https://bugs.torproject.org/tpo/core/tor/2667">2667</a> for details. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40271">40271</a>.</li>
</ul>
</li>
<li>Minor features (command line):
<ul>
<li>Add long format name "--torrc-file" equivalent to the existing command-line option "-f". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40324">40324</a>. Patch by Daniel Pinto.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (dormant mode):
<ul>
<li>Add a new 'DormantTimeoutEnabled' option to allow coarse-grained control over whether the client ever becomes dormant from inactivity. Most people won't need this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40228">40228</a>.</li>
</ul>
</li>
<li>Minor features (fallback directory list):
<ul>
<li>Regenerate the list of fallback directories to contain a new set of 200 relays. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40265">40265</a>.</li>
</ul>
</li>
<li>Minor features (geoip data):
<ul>
<li>Update the geoip files to match the IPFire Location Database, as retrieved on 2021/04/13.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Edit heartbeat log messages so that more of them begin with the string "Heartbeat: ". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40322">40322</a>; patch from 'cypherpunks'.</li>
</ul>
</li>
<li>Minor bugfixes (bridge, pluggable transport):
<ul>
<li>Fix a regression that made it impossible start Tor using a bridge line with a transport name and no fingerprint. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40360">40360</a>; bugfix on 0.4.5.4-rc.</li>
</ul>
</li>
<li>Minor bugfixes (channel, DoS):
<ul>
<li>Fix a non-fatal BUG() message due to a too-early free of a string, when listing a client connection from the DoS defenses subsystem. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40345">40345</a>; bugfix on 0.4.3.4-rc.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation warning about unused functions when building with a libc that lacks the GLOB_ALTDIRFUNC constant. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40354">40354</a>; bugfix on 0.4.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (configuration):
<ul>
<li>Fix pattern-matching for directories on all platforms when using %include options in configuration files. This patch also fixes compilation on musl libc based systems. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40141">40141</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Move the "overload-general" line from extrainfo to the server descriptor. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40364">40364</a>; bugfix on 0.4.6.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, BSD):
<ul>
<li>Fix pattern-matching errors when patterns expand to invalid paths on BSD systems. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40318">40318</a>; bugfix on 0.4.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Documentation (manual):
<ul>
<li>Move the ServerTransport* options to the "SERVER OPTIONS" section. Closes issue <a href="https://bugs.torproject.org/tpo/core/tor/40331">40331</a>.</li>
<li>Indicate that the HiddenServiceStatistics option also applies to bridges. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40346">40346</a>.</li>
<li>Move the description of BridgeRecordUsageByCountry to the section "STATISTICS OPTIONS". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40323">40323</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291752"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291752" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Marco Calamari (not verified)</span> said:</p>
      <p class="date-time">May 07, 2021</p>
    </div>
    <a href="#comment-291752">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291752" class="permalink" rel="bookmark">Great and needed tool!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great and needed tool! Cheers</p>
<p>But IMNSHO there are too few information for expert, and for beginner the wording of messages is **terrible**. You consider this an "all OK" understandable by everyone?</p>
<p>"Third generation onion services – running private services (e.g. websites) that are only accessible through the Tor network.<br />
Recently, based on average data, it looks like this system has gone down for about 1830 minutes at a time. "</p>
<p>No green/yellow/red light? </p>
<p>And for expert why not an easy way to dig down information using links when problem arise?</p>
<p>Thanks for the great job!</p>
</div>
  </div>
</article>
<!-- Comment END -->
