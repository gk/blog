title: Tor at the Heart: Whonix
---
pub_date: 2016-12-27
---
author: ssteele
---
tags:

heart of Internet freedom
Whonix
---
_html_body:

<p><strong>UPDATE, AUGUST 2020 - Since the writing of this blog post, the Tor Project Community has become increasingly concerned by reports of a pattern of tolerance for sexism, racism, and other bigotry within the Whonix community. Therefore, we can no longer endorse Whonix, and do not encourage others to get involved with them. We want to foster a diverse, inclusive, and welcoming environment for all and we feel that associating with Whonix jeopardizes these goals.</strong></p>

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog27" rel="nofollow">Donate today!</a></p>

<p><b>Whonix</b></p>

<p>Whonix is a privacy ecosystem that utilizes compartmentalization to provide a private, leak-resistant environment for many desktop computing activities. Whonix helps users use their favorite desktop applications anonymously. A web browser, IRC client, word processor, and more come pre-installed with safe defaults, and users can safely install custom applications and personalize their desktops with Whonix.</p>

<p>Whonix is designed to run inside a VM and to be paired with Tor. Whonix is composed of two or more virtual machines that run on top of an existing operating system. The primary purpose of this design is to isolate the critical Tor software from the risk-laden environments that often host user-applications, such as email clients and web browsers. Whonix consists of two parts: the first part solely runs Tor and acts as a gateway for a user's Internet traffic, called <i>Whonix-Gateway</i>. The other, called <i>Whonix-Workstation</i>, is for a user's work and is located on a completely isolated network. Even if the user's workstation is compromised with root privileges, it cannot easily reveal IP addresses or leak DNS requests or bypass Tor, because it has neither full knowledge nor control over where and how its traffic is routed. This is <a href="https://en.wikipedia.org/wiki/Air_gap_(networking)" rel="nofollow">security by isolation</a>, and it averts many threats posed by malware, misbehaving applications, and user error.</p>

<p>One of Whonix's core strengths is its flexibility. Whonix can run on Linux, MacOS, or Windows. It can torrify nearly any application's traffic running on nearly any operating system, and it doesn't depend on the application's cooperation. It can even isolate a server behind a Tor Hidden Service running on a separate OS. It can route traffic over VPNs, SSH tunnels, SOCKS proxies, and major anonymity networks, giving users flexibility in their system setups.</p>

<p>Whonix was originally built around compatibility-focused <a href="https://www.virtualbox.org" rel="nofollow">Virtualbox</a>, then time-tested <a rel="nofollow">KVM</a> was added as an option. Now Whonix is shipped-by-default with the advanced, security-focused virtualization platform <a href="https://www.qubes-os.org/" rel="nofollow">QubesOS</a>. Whonix even supports <a href="https://www.qubes-os.org/doc/dispvm/" rel="nofollow">Qubes' DisposableVMs</a>.</p>

<p>Whonix has a safe default configuration that includes a restrictive firewall, privacy-enhanced settings for Debian, <a href="http://wiki.apparmor.net" rel="nofollow">AppArmor</a> profiles, and pre-configured and stream isolated applications.</p>

<p>The Whonix team is currently focused on improving usability for new Whonix users. A Quick-Start Guide will be available shortly to allow users to install and try Whonix on most existing systems.</p>

<p>Whonix is based in Germany but has users and developers from around the world. Like many open-source projects, Whonix depends on the donations and contributions of supporters. It's easy to <a href="https://www.whonix.org/wiki/Contribute" rel="nofollow">get involved</a>!</p>

---
_comments:

<a id="comment-228912"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228912" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228912">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228912" class="permalink" rel="bookmark">YAAAAY! Finally a blog post</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>YAAAAY! Finally a blog post on Whonix :D</p>
<p>By the way they are looking for a new developer now</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229080"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229080" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228912" class="permalink" rel="bookmark">YAAAAY! Finally a blog post</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229080">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229080" class="permalink" rel="bookmark">I skimmed their blog and a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I skimmed their blog and a couple of their forums but didn't see anything about a new dev spot. Can you link? Not a dev myself, just want to see more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229139"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229139" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229080" class="permalink" rel="bookmark">I skimmed their blog and a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229139">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229139" class="permalink" rel="bookmark">Look at their twitter</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Look at their twitter account <a href="https://twitter.com/Whonix/status/812932025594191875" rel="nofollow">https://twitter.com/Whonix/status/812932025594191875</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-228919"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228919" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228919">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228919" class="permalink" rel="bookmark">Who doesn&#039;t like Qubes OS? :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Who doesn't like Qubes OS? :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228929"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228929" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228929">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228929" class="permalink" rel="bookmark">Is it really German i</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it really German i thought the main developer was from or living in Austria.<br />
Would be great if you make another Tor at Heart about Qubes and Tails</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228942" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228942" class="permalink" rel="bookmark">hi
how to adjust bridges in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi<br />
how to adjust bridges in Whonix ???</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229035"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229035" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228942" class="permalink" rel="bookmark">hi
how to adjust bridges in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229035">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229035" class="permalink" rel="bookmark">Hi,
To adjust your bridges</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>To adjust your bridges in Whonix you need to edit the torrc file. If you want to have obfs4 bridges you should add:</p>
<p>UseBridges 1</p>
<p>ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy managed</p>
<p>Bridge obfs4 ........</p>
<p>Hope that helps!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-229038"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229038" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228942" class="permalink" rel="bookmark">hi
how to adjust bridges in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229038">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229038" class="permalink" rel="bookmark">https://www.whonix.org/wiki/B</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.whonix.org/wiki/Bridges" rel="nofollow">https://www.whonix.org/wiki/Bridges</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229254"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229254" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229038" class="permalink" rel="bookmark">https://www.whonix.org/wiki/B</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229254">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229254" class="permalink" rel="bookmark">too many words ! more</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>too many words ! more pictures !   &gt;__&lt;</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-228974"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228974" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228974">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228974" class="permalink" rel="bookmark">why have not they chosen</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why have not they chosen Tomoyo (apparmor is buggy) ?<br />
i did not read that whonix could be installed (is it not a live distro ?).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229453"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229453" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228974" class="permalink" rel="bookmark">why have not they chosen</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229453">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229453" class="permalink" rel="bookmark">Unpopular. Too few usage</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unpopular. Too few usage examples. It's a volunteer project. No one contributed that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-229022"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229022" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-229022">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229022" class="permalink" rel="bookmark">firefox is not secure</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>firefox is not secure especially for china user,for firefox (en) will disable extension search function after auto update in china but good before update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229454"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229454" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229022" class="permalink" rel="bookmark">firefox is not secure</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229454">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229454" class="permalink" rel="bookmark">Whonix uses Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whonix uses Tor Browser which at the moment is the most sophisticated browser for privacy and anonymity.</p>
<p><a href="https://www.whonix.org/wiki/Tor_Browser" rel="nofollow">https://www.whonix.org/wiki/Tor_Browser</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-229023"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229023" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-229023">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229023" class="permalink" rel="bookmark">so do not use tor to bandage</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>so do not use tor to bandage with firefox,but google.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-229039"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229039" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2016</p>
    </div>
    <a href="#comment-229039">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229039" class="permalink" rel="bookmark">Whonix is a general purpose</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whonix is a general purpose OS operating as two VMs: Tor Gateway VM and Tor Workstation (all apps can be torrified in the workstation). It also has stream isolation to make sure apps don't use the same Tor circuits.</p>
<p><a href="https://www.whonix.org/wiki/Comparison_with_Others" rel="nofollow">https://www.whonix.org/wiki/Comparison_with_Others</a></p>
<p>Basically Whonix or Qubes-Whonix is what most Tor Browser users should be defaulting to, unless they want their ass hacked in the New Year.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-229046"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229046" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2016</p>
    </div>
    <a href="#comment-229046">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229046" class="permalink" rel="bookmark">no whonix is a virtual</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>no whonix is a virtual machine image to be run within virtualbox/vmware or Qubes OS</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229456"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229456" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229046" class="permalink" rel="bookmark">no whonix is a virtual</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229456">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229456" class="permalink" rel="bookmark">Whonix in VMware is not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whonix in VMware is not really maintained at this time.</p>
<p><a href="https://www.whonix.org/wiki/VMware" rel="nofollow">https://www.whonix.org/wiki/VMware</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-229056"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229056" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2016</p>
    </div>
    <a href="#comment-229056">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229056" class="permalink" rel="bookmark">&gt; Whonix was originally</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Whonix was originally built around compatibility-focused Virtualbox, then time-tested KVM was added as an option. Now Whonix is shipped-by-default with the advanced, security-focused virtualization platform QubesOS. Whonix even supports Qubes' DisposableVMs.</p>
<p>KVM is a pretty nice option, but doesn't QubesOS use Xen for virtualization? That said, how does Whonix work under QubesOS, given that neither KVM nor Virtualbox works under Xen? Unless Whonix is able to detect and use Xen instead of KVM/Virtualbox under the hood?</p>
<p>In my opinion, Xen is the way to go for security. It supports things like FLASK (similar to Linux Security Modules, for the Xen hypervisor), networking domains (unprivileged virtual machines that only have access to networking hardware, the Dom0 (administrative domain) can be air-gapped), and some hardware drivers can be run in their own unprivileged mini-VMs (called Stub Domains). It supports paravirtualization for security and performance, and hardware virtualization (i.e. QEMU) for compatibility, and the QEMU emulator can even be run inside its own unprivileged paravirtual VM. If your processor has an IOMMU (Intel VT-d), Xen can even isolate DMA access by hardware assigned to an unprivileged VM. It's a really underappreciated project in my opinion.</p>
<p>On the other hand, I guess if you're just using Whonix as an application you install in any OS, Xen would be very cumbersome and difficult to setup for that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229195"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229195" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229056" class="permalink" rel="bookmark">&gt; Whonix was originally</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229195">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229195" class="permalink" rel="bookmark">There are seperate</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are seperate images/versions for KVM, Virtualbox and Qubes. The Qubes one is considered to be the safest and also is the easiest to set up.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229451"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229451" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229195" class="permalink" rel="bookmark">There are seperate</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229451">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229451" class="permalink" rel="bookmark">Thanks. So is Whonix just a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks. So is Whonix just a guest OS image then? And not an application you install inside the host also?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-229840"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229840" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229451" class="permalink" rel="bookmark">Thanks. So is Whonix just a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229840">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229840" class="permalink" rel="bookmark">Yes, OS images for various</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, OS images for various virtualizers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-229208"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229208" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229056" class="permalink" rel="bookmark">&gt; Whonix was originally</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229208">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229208" class="permalink" rel="bookmark">is it not too much</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is it not too much complicated ?<br />
if whonix is not compatible with my soft &amp; my hard ... i mean that it must work under any desktop choice xen &amp; kde &amp; gnome &amp; flubox etc. and if i have intel vt-d ; it must better compatible &amp; that without trouble, bug , ... it is not user-friendly and need too much tweak, care etc. i prefer the torproject : sandbox tor.(virtualprotection).<br />
intel vt-d was made for communicating inside a platform over the world and manage a lot of machine and this special embedded function is a really underappreciated project in my opinion.... maybe a tor dev will know how to join the both for improving sandbox tor in a near future.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-229239"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229239" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-229056" class="permalink" rel="bookmark">&gt; Whonix was originally</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-229239">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229239" class="permalink" rel="bookmark">Very interesting! I also</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very interesting! I also think Xen is really underappreciated project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-229457"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-229457" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2016</p>
    </div>
    <a href="#comment-229457">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-229457" class="permalink" rel="bookmark">Qubes (and therefore</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Qubes (and therefore Qubes-Whonix also) does make use of Xen, IOMMU, Intel VT-d, isolate DMA access.</p>
<p>Whonix was ported to Qubes. Called Qubes-Whonix. It is officially supported.</p>
<p>There is also Whonix for VirtualBox and KVM.</p>
</div>
  </div>
</article>
<!-- Comment END -->
