title: Tor 0.2.8.12 is released
---
pub_date: 2016-12-19
---
author: nickm
---
tags:

tor
stable
release
oldstable
---
categories:

network
releases
---
_html_body:

<p>There's a new "old stable" release of Tor!  (But maybe you want the 0.2.9.8 release instead; that also comes out today.) </p>

<p>Tor 0.2.8.12 backports a fix for a medium-severity issue (bug <a href="https://bugs.torproject.org/21018" rel="nofollow">21018</a> below) where Tor clients could crash when attempting to visit a hostile hidden service. Clients are recommended to upgrade as packages become available for their systems.<br />
It also includes an updated list of fallback directories, backported from 0.2.9.<br />
Now that the Tor 0.2.9 series is stable, only major bugfixes will be backported to 0.2.8 in the future.<br />
You can download Tor 0.2.8 -- and other older release series -- from <a href="https://dist.torproject.org/" rel="nofollow">dist.torproject.org</a>.</p>

<h2>Changes in version 0.2.8.12 - 2016-12-19</h2>

<ul>
<li>Major bugfixes (parsing, security, backported from 0.2.9.8):
<ul>
<li>Fix a bug in parsing that could cause clients to read a single byte past the end of an allocated region. This bug could be used to cause hardened clients (built with --enable-expensive-hardening) to crash if they tried to visit a hostile hidden service. Non- hardened clients are only affected depending on the details of their platform's memory allocator. Fixes bug <a href="https://bugs.torproject.org/21018" rel="nofollow">21018</a>; bugfix on 0.2.0.8-alpha. Found by using libFuzzer. Also tracked as TROVE- 2016-12-002 and as CVE-2016-1254.
  </li>
</ul>
</li>
<li>Minor features (fallback directory list, backported from 0.2.9.8):
<ul>
<li>Replace the 81 remaining fallbacks of the 100 originally introduced in Tor 0.2.8.3-alpha in March 2016, with a list of 177 fallbacks (123 new, 54 existing, 27 removed) generated in December 2016. Resolves ticket <a href="https://bugs.torproject.org/20170" rel="nofollow">20170</a>.
  </li>
</ul>
</li>
<li>Minor features (geoip, backported from 0.2.9.7-rc):
<ul>
<li>Update geoip and geoip6 to the December 7 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
</ul>

