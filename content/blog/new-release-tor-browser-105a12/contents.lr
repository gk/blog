title: New Release: Tor Browser 10.5a12 (Android Only)
---
pub_date: 2021-03-21
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a12 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a12/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable release for <a href="https://blog.torproject.org/new-release-tor-browser-10012">Android</a> instead.</p>
<p>This release updates Fenix to 87.0.0-beta.2. Additionally, we update NoScript to 11.2.3 and Tor to 0.4.6.1-alpha.</p>
<p><b>Note</b>: This is the first Tor Browser version that does <b>not</b> support version 2 onion services. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a11 is:</p>
<ul>
<li>Android
<ul>
<li>Update Fenix to 87.0.0-beta.2</li>
<li>Update NoScript to 11.2.3</li>
<li>Update Tor to 0.4.6.1-alpha</li>
<li>Translations update</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40030">Bug 40030</a>: DuckDuckGo redirect to html doesn't work</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40043">Bug 40043</a>: Rebase android-components patches for Fenix 87 beta 2 builds</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40045">Bug 40045</a>: Add External App Prompt for Sharing Images</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40150">Bug 40150</a>: Rebase Fenix patches to Fenix 87 beta 2</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40361">Bug 40361</a>: Rebase tor-browser patches to 87.0b4</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li>Update Go to 1.15.10</li>
<li><a href="https://bugs.torproject.org/23631">Bug 23631</a>: Use rootless containers</li>
<li><a href="https://bugs.torproject.org/tpo/applications/rbm/40016">Bug 40016</a>: getfpaths is not setting origin_project</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40172">Bug 40172</a>: Move Gradle compilers out of android-toolchain to own gradle project</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40241">Bug 40241</a>: Update components for mozilla87-based Fenix</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291388"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291388" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2021</p>
    </div>
    <a href="#comment-291388">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291388" class="permalink" rel="bookmark">so will v2 onions be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>so will v2 onions be deprecated in the next stable release, 10.0.14?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291393"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291393" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291388" class="permalink" rel="bookmark">so will v2 onions be…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291393">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291393" class="permalink" rel="bookmark">No, v2 addresses will…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, v2 addresses will continue working in the stable series (10.0) until July.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291391"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291391" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 23, 2021</p>
    </div>
    <a href="#comment-291391">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291391" class="permalink" rel="bookmark">You still haven&#039;t removed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You still haven't removed the spyware level permissions, Tor activity on a browser which has GPS and microphone access is destin for failure. I know the GPS option is switched off by default but it's more than fair to assume that there are exploits for leveraging this in the wild. It's been a known issue since the end of last year so that has given hackers/blackmailers and agencies plenty of time to craft and trial various methods. Do something.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291395"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291395" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291391" class="permalink" rel="bookmark">You still haven&#039;t removed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291395">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291395" class="permalink" rel="bookmark">In more recent versions of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In more recent versions of Android (I think 8 and up, or so?), you can go in to settings and selectively override certain permissions of each individual app. On my phone (LineageOS) I have Tor Browser set to "always ask" for coarse location, fine location, camera, and mic.</p>
<p><a href="https://web.archive.org/web/20210324174918/https://support.google.com/android/answer/9431959?hl=en" rel="nofollow">https://web.archive.org/web/20210324174918/https://support.google.com/a…</a></p>
<p>BTW, the permissions are Mozilla's fault, not Tor Project's fault.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291397" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291391" class="permalink" rel="bookmark">You still haven&#039;t removed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291397" class="permalink" rel="bookmark">Why not do something…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why not do something yourself? You can find all the code and configurations here: <a href="https://gitlab.torproject.org" rel="nofollow">gitlab.torproject.org</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291391" class="permalink" rel="bookmark">You still haven&#039;t removed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291407" class="permalink" rel="bookmark">https://gitlab.torproject…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40109" rel="nofollow">https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40109</a><br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/30604" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/306…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291517"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291517" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Cindy (not verified)</span> said:</p>
      <p class="date-time">April 04, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291391" class="permalink" rel="bookmark">You still haven&#039;t removed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291517">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291517" class="permalink" rel="bookmark">I hardly know what the heck…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hardly know what the heck this Tor browser is all about but I'm waiting for it because I know we need it. I have Cyberghost/VPN. Will that get in the way or help? I'm just an old gal who's pissed off at what's going on. Thanks for finding these bugs and such.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291392"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291392" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon2847 (not verified)</span> said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
    <a href="#comment-291392">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291392" class="permalink" rel="bookmark">v2 links are not loading on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>v2 links are not loading on Android on this version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291394"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291394" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291392" class="permalink" rel="bookmark">v2 links are not loading on…</a> by <span>Anon2847 (not verified)</span></p>
    <a href="#comment-291394">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291394" class="permalink" rel="bookmark">Yes, please see the &quot;note&quot;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, please see the "note" included in the post.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291396"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291396" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
    <a href="#comment-291396">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291396" class="permalink" rel="bookmark">Is it possible to change the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it possible to change the SOCKSPort/ControlPort used by the alpha version so that it can run alongside the stable version? If I try to run both at the same time I get an error about "failed to bind to one of the listener ports". It would be nice if we could have both open at the same time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291402" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291396" class="permalink" rel="bookmark">Is it possible to change the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291402" class="permalink" rel="bookmark">Yes, we would like that too…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, we would like that too. We have a general ticket for all platforms: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/28810" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/288…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291408" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291396" class="permalink" rel="bookmark">Is it possible to change the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291408" class="permalink" rel="bookmark">Support:
Can I run multiple…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Support:<br />
<a href="https://support.torproject.org/tbb/tbb-36/" rel="nofollow">Can I run multiple instances of Tor Browser?</a> -- "We do not recommend running multiple instances of Tor Browser, and doing so may not work as anticipated on many platforms."</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>onion (not verified)</span> said:</p>
      <p class="date-time">March 30, 2021</p>
    </div>
    <a href="#comment-291486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291486" class="permalink" rel="bookmark">New version Tor Browser for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New version Tor Browser for Android alpha 10.5a12, no work site onion!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291567"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291567" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291486" class="permalink" rel="bookmark">New version Tor Browser for…</a> by <span>onion (not verified)</span></p>
    <a href="#comment-291567">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291567" class="permalink" rel="bookmark">Yes, see the note in the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, see the note in the original post:</p>
<p>Note: This is the first Tor Browser version that does not support version 2 onion services. Please see the previously published deprecation timeline.</p>
<p><a href="https://blog.torproject.org/v2-deprecation-timeline" rel="nofollow">https://blog.torproject.org/v2-deprecation-timeline</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
